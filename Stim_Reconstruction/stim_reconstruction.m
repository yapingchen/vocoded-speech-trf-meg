function stim_reconstruction(subject, outdir)
% modified from c07_bm_sensor_ds50_clean
%% init obob_ownft...
addpath('/mnt/obob/obob_ownft');
obob_init_ft;

%% init mtrf
addpath('/mnt/obob/staff/ychen/vocoding_5levels/analysis/')
addpath('/mnt/obob/staff/ychen/tools/mTRF_Toolbox_2/mtrf/')

%% preproc data
nojumpdir = '/mnt/obob/staff/ychen/vocoding_5levels/data/preproc/';
nobigjump_fname = fullfile(nojumpdir, sprintf('%s_nojump.mat', subject));
load(nobigjump_fname, 'data_clean');

% time information in each trial was messed up during preprocessing
for i=1:length(data_clean.trial)
data_clean.time{i} = [0:0.005:(0.005*399)];
end

% remove trial with silence preriod longer than 300 ms 
cfg = [];
cfg.sduration = 300;
short_silence_trial = functions.find_short_silence_trial_new(cfg, data_clean);

cfg = [];
cfg.trials = short_silence_trial;
data_short = ft_selectdata(cfg, data_clean);
clear data_clean

% low-pass filter
cfg = [];
cfg.continuous = 'yes';
cfg.lpfilter = 'yes';
cfg.lpfilttype = 'firws'; % (default = 'but')
cfg.lpfiltwintype = 'kaiser'; % (default = 'hamming')
cfg.lpfreq = 8;
data_ep = ft_preprocessing(cfg, data_short);
data_ep = rmfield(data_ep , 'sampleinfo');

% resample data to speed up
cfg = [];
cfg.resamplefs = 50;
cfg.detrend = 'no';
data_rs = ft_resampledata(cfg, data_ep);
data_rs.grad = data_ep.grad;
data_rs.elec = data_ep.elec;
data_rs.hdr = data_ep.hdr;
clear data_ep

data_clean = data_rs;

%% define conditions
ind_val=data_clean.trialinfo;
Trlnonoise = find(ind_val>=51 & ind_val<=74); Trlnonoise= sort(Trlnonoise);
Trl7ch = find(ind_val>=81 & ind_val<=104); Trl7ch =sort(Trl7ch);
Trl5ch = find(ind_val>=111 & ind_val<=134); Trl5ch =sort(Trl5ch);
Trl3ch = find(ind_val>=141 & ind_val<=164); Trl3ch =sort(Trl3ch);
Trl2ch = find(ind_val>=171 & ind_val<=194); Trl2ch =sort(Trl2ch);
Trl1ch = find(ind_val>=201 & ind_val<=224); Trl1ch =sort(Trl1ch);

%% get lowest number of trials
nTrials = min([length(Trlnonoise), length(Trl7ch), length(Trl5ch), length(Trl3ch), length(Trl2ch), length(Trl1ch)]);
Trlnonoise =(sort(Trlnonoise(randperm(length(Trlnonoise),nTrials))));
Trl7ch =(sort(Trl7ch(randperm(length(Trl7ch),nTrials))));
Trl5ch =(sort(Trl5ch(randperm(length(Trl5ch),nTrials))));
Trl3ch =(sort(Trl3ch(randperm(length(Trl3ch),nTrials))));
Trl2ch =(sort(Trl2ch(randperm(length(Trl2ch),nTrials))));
Trl1ch =(sort(Trl1ch(randperm(length(Trl1ch),nTrials))));

%% get response data
cfg = [];
cfg.channel = 'MEG';
cfg.trials = Trlnonoise;
data_no = ft_selectdata(cfg, data_clean);

cfg.trials = Trl7ch;
data_7ch = ft_selectdata(cfg, data_clean);

cfg.trials = Trl5ch;
data_5ch = ft_selectdata(cfg, data_clean);

cfg.trials = Trl3ch;
data_3ch = ft_selectdata(cfg, data_clean);

cfg.trials = Trl2ch;
data_2ch = ft_selectdata(cfg, data_clean);

cfg.trials = Trl1ch;
data_1ch = ft_selectdata(cfg, data_clean);

%% get stim data
cfg = [];
cfg.channel = 'speech_envelope';
cfg.trials = Trlnonoise;
stim_no = ft_selectdata(cfg, data_clean);

cfg.trials = Trl7ch;
stim_7ch = ft_selectdata(cfg, data_clean);

cfg.trials = Trl5ch;
stim_5ch = ft_selectdata(cfg, data_clean);

cfg.trials = Trl3ch;
stim_3ch = ft_selectdata(cfg, data_clean);

cfg.trials = Trl2ch;
stim_2ch = ft_selectdata(cfg, data_clean);

cfg.trials = Trl1ch;
stim_1ch = ft_selectdata(cfg, data_clean);


%% Partition data properly
%transpose all trials to fit and zscore
data_no.trial = cellfun(@(x) transpose(zscore(x,0,2)), data_no.trial, 'UniformOutput', false);
data_7ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), data_7ch.trial, 'UniformOutput', false);
data_5ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), data_5ch.trial, 'UniformOutput', false);
data_3ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), data_3ch.trial, 'UniformOutput', false);
data_2ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), data_2ch.trial, 'UniformOutput', false);
data_1ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), data_1ch.trial, 'UniformOutput', false);

%zscore speech envelope
stim_no.trial = cellfun(@(x) transpose(zscore(x,0,2)), stim_no.trial, 'UniformOutput', false);
stim_7ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), stim_7ch.trial, 'UniformOutput', false);
stim_5ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), stim_5ch.trial, 'UniformOutput', false);
stim_3ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), stim_3ch.trial, 'UniformOutput', false);
stim_2ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), stim_2ch.trial, 'UniformOutput', false);
stim_1ch.trial = cellfun(@(x) transpose(zscore(x,0,2)), stim_1ch.trial, 'UniformOutput', false);


%% partition (reformat data to fit to function)
nfold = 5;
testTrial = 2; %always take the same trial as test trial for all subjects
[stim_train_no, resp_train_no, stim_test_no, resp_test_no] = mTRFpartition(vertcat(stim_no.trial{:}), vertcat(data_no.trial{:}), nfold, testTrial);
[stim_train_7ch, resp_train_7ch, stim_test_7ch, resp_test_7ch] = mTRFpartition(vertcat(stim_7ch.trial{:}), vertcat(data_7ch.trial{:}), nfold, testTrial);
[stim_train_5ch, resp_train_5ch, stim_test_5ch, resp_test_5ch] = mTRFpartition(vertcat(stim_5ch.trial{:}), vertcat(data_5ch.trial{:}), nfold, testTrial);
[stim_train_3ch, resp_train_3ch, stim_test_3ch, resp_test_3ch] = mTRFpartition(vertcat(stim_3ch.trial{:}), vertcat(data_3ch.trial{:}), nfold, testTrial);
[stim_train_2ch, resp_train_2ch, stim_test_2ch, resp_test_2ch] = mTRFpartition(vertcat(stim_2ch.trial{:}), vertcat(data_2ch.trial{:}), nfold, testTrial);
[stim_train_1ch, resp_train_1ch, stim_test_1ch, resp_test_1ch] = mTRFpartition(vertcat(stim_1ch.trial{:}), vertcat(data_1ch.trial{:}), nfold, testTrial);

%% run crossvalidation a la Crosse
fs = 50; 
Dir = -1;
tmin = -150; 
tmax = 450; 
lambda = 10.^(0:1:6); 

%% Do the crossvalidation
[bwd.cv_stats_no, ~] = mTRFcrossval(stim_train_no, resp_train_no,fs,Dir,tmin,tmax,lambda);
[bwd.cv_stats_7ch, ~] = mTRFcrossval(stim_train_7ch, resp_train_7ch,fs,Dir,tmin,tmax,lambda);
[bwd.cv_stats_5ch, ~] = mTRFcrossval(stim_train_5ch, resp_train_5ch,fs,Dir,tmin,tmax,lambda);
[bwd.cv_stats_3ch, ~] = mTRFcrossval(stim_train_3ch, resp_train_3ch,fs,Dir,tmin,tmax,lambda);
[bwd.cv_stats_2ch, ~] = mTRFcrossval(stim_train_2ch, resp_train_2ch,fs,Dir,tmin,tmax,lambda);
[bwd.cv_stats_1ch, ~] = mTRFcrossval(stim_train_1ch, resp_train_1ch,fs,Dir,tmin,tmax,lambda);


%% Pick the best lambda
cfg = [];
cfg.lambdas = lambda;
lambda_idx_no = functions.find_optimal_lambda(cfg, mean(bwd.cv_stats_no.err));
lambda_idx_7ch = functions.find_optimal_lambda(cfg, mean(bwd.cv_stats_7ch.err));
lambda_idx_5ch = functions.find_optimal_lambda(cfg, mean(bwd.cv_stats_5ch.err));
lambda_idx_3ch = functions.find_optimal_lambda(cfg, mean(bwd.cv_stats_3ch.err));
lambda_idx_2ch = functions.find_optimal_lambda(cfg, mean(bwd.cv_stats_2ch.err));
lambda_idx_1ch = functions.find_optimal_lambda(cfg, mean(bwd.cv_stats_1ch.err));

%% Do the training
bwd.model_no = mTRFtrain(stim_train_no, resp_train_no, fs, Dir, tmin, tmax, lambda(lambda_idx_no));
bwd.model_7ch = mTRFtrain(stim_train_7ch, resp_train_7ch, fs, Dir, tmin, tmax, lambda(lambda_idx_7ch));
bwd.model_5ch = mTRFtrain(stim_train_5ch, resp_train_5ch, fs, Dir, tmin, tmax, lambda(lambda_idx_5ch));
bwd.model_3ch = mTRFtrain(stim_train_3ch, resp_train_3ch, fs, Dir, tmin, tmax, lambda(lambda_idx_3ch));
bwd.model_2ch = mTRFtrain(stim_train_2ch, resp_train_2ch, fs, Dir, tmin, tmax, lambda(lambda_idx_2ch));
bwd.model_1ch = mTRFtrain(stim_train_1ch, resp_train_1ch, fs, Dir, tmin, tmax, lambda(lambda_idx_1ch));

%% Do the prediction
[bwd.pred_no.pred, bwd.pred_no.test] = mTRFpredict(stim_test_no, resp_test_no, bwd.model_no);
[bwd.pred_7ch.pred, bwd.pred_7ch.test] = mTRFpredict(stim_test_7ch, resp_test_7ch, bwd.model_7ch);
[bwd.pred_5ch.pred, bwd.pred_5ch.test] = mTRFpredict(stim_test_5ch, resp_test_5ch, bwd.model_5ch);
[bwd.pred_3ch.pred, bwd.pred_3ch.test] = mTRFpredict(stim_test_3ch, resp_test_3ch, bwd.model_3ch);
[bwd.pred_2ch.pred, bwd.pred_2ch.test] = mTRFpredict(stim_test_2ch, resp_test_2ch, bwd.model_2ch);
[bwd.pred_1ch.pred, bwd.pred_1ch.test] = mTRFpredict(stim_test_1ch, resp_test_1ch, bwd.model_1ch);

%% Save the envelope used for testing
bwd.pred_no.stim = stim_test_no;
bwd.pred_7ch.stim = stim_test_7ch;
bwd.pred_5ch.stim = stim_test_5ch;
bwd.pred_3ch.stim = stim_test_3ch;
bwd.pred_2ch.stim = stim_test_2ch;
bwd.pred_1ch.stim = stim_test_1ch;

%% save
out_fname = fullfile(outdir, sprintf('%s_bm.mat', subject));
save(out_fname, '-v7.3', 'bwd');
