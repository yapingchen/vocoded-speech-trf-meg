clear all;
wkspace='/mnt';
%wkspace='/Volumes';
%%
addpath(fullfile(wkspace, '/obob/obob_ownft'));
% addpath(fullfile(wkspace, '/obob/staff/ahauswald/lip_noise_5/'));

cfg = [];
cfg.package.svs = true;
obob_init_ft(cfg);
%%
cd '/mnt/obob/staff/ychen/vocoding_5levels/analysis/coh_forfooof'
study_acronym = 'yc_audiobook_vocoding';
all_subjects = subjectsfromsinuhe(study_acronym);
indir = '/mnt/obob/staff/ychen/vocoding_5levels/data/coh_forFooof_yc_1_20Hz_4s_nohp_rejectz50/';

for i=1:length(all_subjects)
  load(fullfile(indir, sprintf('%s_audio.mat', all_subjects{i})));
  labels={'speech_envelope'};
  
    coh_novoc.speech_envelope.powspctrm=coh_novoc.speech_envelope.cohspctrm;
    coh_chan7.speech_envelope.powspctrm=coh_chan7.speech_envelope.cohspctrm;
    coh_chan5.speech_envelope.powspctrm=coh_chan5.speech_envelope.cohspctrm;
    coh_chan3.speech_envelope.powspctrm=coh_chan3.speech_envelope.cohspctrm;
    coh_chan2.speech_envelope.powspctrm=coh_chan2.speech_envelope.cohspctrm;
    coh_chan1.speech_envelope.powspctrm=coh_chan1.speech_envelope.cohspctrm;
    
    coh_novoc.speech_envelope.label={coh_novoc.speech_envelope.labelcmb{:,1}}';
    coh_chan7.speech_envelope.label={coh_chan7.speech_envelope.labelcmb{:,1}}';
    coh_chan5.speech_envelope.label={coh_chan5.speech_envelope.labelcmb{:,1}}';
    coh_chan3.speech_envelope.label={coh_chan3.speech_envelope.labelcmb{:,1}}';
    coh_chan2.speech_envelope.label={coh_chan2.speech_envelope.labelcmb{:,1}}';
    coh_chan1.speech_envelope.label={coh_chan1.speech_envelope.labelcmb{:,1}}';
    
    fields2remove = {'labelcmb', 'cohspctrm'};
    coh_novoc.speech_envelope = rmfield(coh_novoc.speech_envelope, fields2remove);
    coh_chan7.speech_envelope = rmfield(coh_chan7.speech_envelope, fields2remove);
    coh_chan5.speech_envelope = rmfield(coh_chan5.speech_envelope, fields2remove);
    coh_chan3.speech_envelope = rmfield(coh_chan3.speech_envelope, fields2remove);
    coh_chan2.speech_envelope = rmfield(coh_chan2.speech_envelope, fields2remove);
    coh_chan1.speech_envelope = rmfield(coh_chan1.speech_envelope, fields2remove);    
           
    coh_novoc.speech_envelope.dimord='chan_freq';
    coh_chan7.speech_envelope.dimord='chan_freq';
    coh_chan5.speech_envelope.dimord='chan_freq';
    coh_chan3.speech_envelope.dimord='chan_freq';
    coh_chan2.speech_envelope.dimord='chan_freq';
    coh_chan1.speech_envelope.dimord='chan_freq';
    
    novoc{i}= coh_novoc.speech_envelope;
    chan7{i}= coh_chan7.speech_envelope;
    chan5{i}= coh_chan5.speech_envelope;
    chan3{i}= coh_chan3.speech_envelope;
    chan2{i}= coh_chan2.speech_envelope;
    chan1{i}= coh_chan1.speech_envelope;

end
clear coh_*


%% calculate grand averages
cfg=[];
% cfg.channel=novoc{1}.label(ia);
cfg.parameter='powspctrm';

GA_novoc=ft_freqgrandaverage(cfg,  novoc{:});
GA_chan7=ft_freqgrandaverage(cfg,  chan7{:});
GA_chan5=ft_freqgrandaverage(cfg,  chan5{:});
GA_chan3=ft_freqgrandaverage(cfg,  chan3{:});
GA_chan2=ft_freqgrandaverage(cfg,  chan2{:});
GA_chan1=ft_freqgrandaverage(cfg,  chan1{:});
%%  
addpath('/mnt/obob/staff/ahauswald/useful_functions/')

for isub = 1:24
    no_voc(isub,:) = mean(novoc{isub}.powspctrm,1);
    chan_7(isub,:) = mean(chan7{isub}.powspctrm,1);
    chan_5(isub,:) = mean(chan5{isub}.powspctrm,1);
    chan_3(isub,:) = mean(chan3{isub}.powspctrm,1);
    chan_2(isub,:) = mean(chan2{isub}.powspctrm,1);
    chan_1(isub,:) = mean(chan1{isub}.powspctrm,1);
end

no_mean = mean(no_voc,1);
chan7_mean = mean(chan_7,1);
chan5_mean = mean(chan_5,1);
chan3_mean = mean(chan_3,1);
chan2_mean = mean(chan_2,1);
chan1_mean = mean(chan_1,1);

no_err = std(no_voc,1)/sqrt(24);
chan7_err = std(chan_7,1)/sqrt(24);
chan5_err = std(chan_5,1)/sqrt(24);
chan3_err = std(chan_3,1)/sqrt(24);
chan2_err = std(chan_2,1)/sqrt(24);
chan1_err = std(chan_1,1)/sqrt(24);

figure;
shadedErrorBar(novoc{1}.freq, no_mean, no_err,{'k'},1)

hold on
shadedErrorBar(novoc{1}.freq, chan7_mean, chan7_err,{'r'},1)
shadedErrorBar(novoc{1}.freq, chan5_mean, chan5_err,{'g'},1)
shadedErrorBar(novoc{1}.freq, chan3_mean, chan3_err,{'m'},1)
shadedErrorBar(novoc{1}.freq, chan2_mean, chan2_err,{'y'},1)
shadedErrorBar(novoc{1}.freq, chan1_mean, chan1_err,{'b'},1)

%% morey

novoc=cell2mat(novoc)
chan7=cell2mat(chan7)
chan5=cell2mat(chan5)
chan3=cell2mat(chan3)
chan2=cell2mat(chan2)
chan1=cell2mat(chan1)

data_all=(novoc + chan7 + chan5 + chan3 + chan2 + chan1);

%%
figure;
plot(GA_novoc.freq, mean(GA_novoc.powspctrm),'k')
hold
plot(GA_novoc.freq, mean(GA_chan7.powspctrm),'r')
plot(GA_novoc.freq, mean(GA_chan5.powspctrm),'m')
plot(GA_novoc.freq, mean(GA_chan3.powspctrm),'g')
plot(GA_novoc.freq, mean(GA_chan2.powspctrm),'y')
plot(GA_novoc.freq, mean(GA_chan1.powspctrm),'b')

legend('nonov', '7-chan', '5-chan', '3-chan', '2-chan', '1-chan')


%% prepare neighbours
load mni_grid_1_cm_2982pnts.mat
template_grid=ft_convert_units(template_grid, 'm');
template_grid.coordsys = 'mni'; % for new obobft 072020

atlas= ft_read_atlas('/mnt/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii');
atlas = ft_convert_units(atlas,'m');

%% for 1457 virtual sensors
cfg = [];
cfg.atlas = atlas;
cfg.roi = atlas.tissuelabel;
mask = ft_volumelookup(cfg,template_grid);
template_grid.inside(template_grid.inside==1) = 0;
template_grid.inside(mask) = 1;

elec = struct;
elec.chanpos(:,:) = template_grid.pos(template_grid.inside,:);
elec.label = chan1{1}.label(1:1457);

% for 2982 virtual sensors
% elec = [];
% elec.chanpos= template_grid.pos(template_grid.inside,:);
% elec.label=novoc{1}.label;

%%
cfg = [];
cfg.elec = elec;
cfg.method = 'distance';
cfg.neighbourdist = 0.016; % 0.011 for 2982; 0.016 for 1457
neighbour = ft_prepare_neighbours(cfg);

% %%
% for i=1:24
% dummy{i}=chan1{1};
% dummy{i}.powspctrm=ones(size(chan1{1}.powspctrm))*0.018;
% end
%% statistik

nsubj =  length(chan1);

cfg = [];
cfg.statistic = 'depsamplesFunivariate';%test-statistik
%cfg.statistic = 'depsamplesregrT';%test-statistik
cfg.method = 'montecarlo';
cfg.correctm = 'cluster';
%cfg.channel='MEGGRAD';
cfg.numrandomization = 10000;
cfg.neighbours = neighbour;
cfg.minnbchan=3;
cfg.clusteralpha = 0.05; 
cfg.design = [ones(1,nsubj) ones(1,nsubj)*2 ones(1,nsubj)*3 ones(1,nsubj)*4 ones(1,nsubj)*5 ones(1,nsubj)*6; 1:nsubj 1:nsubj 1:nsubj 1:nsubj 1:nsubj 1:nsubj];
%cfg.design = [ones(1,nsubj) ones(1,nsubj)*2 ; 1:nsubj 1:nsubj ];
cfg.ivar = 1;
cfg.uvar = 2;
cfg.frequency = [2 7];
cfg.avgoverfreq = 'yes';
cfg.tail = 1;
cfg.correcttail='prob';
stat = ft_freqstatistics(cfg, novoc{:}, chan7{:}, chan5{:}, chan3{:}, chan2{:}, chan1{:}); %reihenfolge wichtig für Treg

%stat = ft_freqstatistics(cfg, novoc{:}, dummy{:}); %reihenfolge wichtig für Treg

% 
% stat.con=speech_envelope;
% stat.sens=cfg.channel;

%%
stat.clus = stat.stat.*(stat.mask == 1);
%%
cfg = [];
cfg.frequency = [2 7];
cfg.parameter = 'clus';
cfg.sourcegrid = template_grid;
source = obob_svs_virtualsens2source(cfg, stat);

% interpolate the mask
load standard_mri_better
cfg = [];
cfg.parameter = 'clus';
inter_source = ft_sourceinterpolate(cfg, source, mri);
inter_source.coordsys = 'mni';

%%
%inter_source.mask=inter_source.powspctrm >=(max(max(max(inter_source.powspctrm))).*0.80);
inter_source_3.mask=inter_source_3.clus.* inter_source_3.overlap;
inter_source_6.mask=inter_source_6.clus.* inter_source_6.overlap;
%%
cfg = [];
cfg.method = 'ortho';
cfg.funparameter = 'clus';
cfg.maskparameter = 'mask';
%cfg.crosshair = 'yes';
cfg.coordsys = 'mni';
cfg.inputcoord = 'mni';
cfg.interactive = 'yes';
% cfg.funcolorlim = [0 10];
cfg.atlas = atlas;
cfg.atlas.coordsys = 'mni';
% cfg.marker        = template_grid.pos(ia_all,:,1);
 ft_sourceplot(cfg, inter_source);
 
 %%
 addpath (fullfile(wkspace, '/obob/staff/ahauswald/useful_functions'));
%%
cfg = [];
cfg.funparameter = 'clus';
cfg.maskparameter = 'clus';
cfg.method = 'surface';
% cfg.renderer = 'zbuffer';
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'yes';
cfg.camlight = 'no';
cfg.funcolormap    = 'hot';
% make figures for right hemisphere
cfg.surffile = 'surface_white_both.mat'; % surface_inflated_both_caret.mat surface_white_both.mat
cfg.surfdownsample = 10;
ft_sourceplot(cfg, inter_source)
view([90 0])
 
%% voxel restricted by scaling to maximum
%stat.clus = (stat.stat(:,4:7).*(stat.posclusterslabelmat(:,4:7) == 1))>max(max(stat.stat(:,4:7)))*0.5;
stat.clus = stat.mask == 1;
stat.clus2=(sum(stat.clus, 2)>=1);
voxels_1_4 = stat.label(stat.clus2);

load (fullfile('/mnt/obob/staff/ahauswald/useful_functions/mni1457_functionallabels'));

test=cell2mat(voxels_1_4);
test2=str2num(test);

%%
newgrid=template_grid;
newgrid.inside(template_grid.inside==1) = 0;
newgrid.inside(ia_all) = 1;
%%
% ROI frontal
chansel=ft_channelselection(cell({'*Frontal*_L_*' }), label);
%chansel=ft_channelselection(cell({'*Frontal*_L_*' '*Cingulum*_L_*' '*Temporal*_Pole*_L_*'}), label);
chansel=ft_channelselection(cell({'*Supp*_L_*' }), label);


% ROI thalamus right
%chansel=ft_channelselection(cell({'Thalamus*R*' '*Putamen*R*' '*Pallidum*R*' }), label);

% ROI thalamus left
chansel=ft_channelselection(cell({'Thalamus*L*' '*Putamen*L*' '*Pallidum*L*'}), label);

% ROI temporal right
chansel=ft_channelselection(cell({'*Rolandic*_R_*' '*Temporal*_Sup*_R_*'  'Postcentral*R*' '*Heschl*R*'}), label);

% ROI temporal left
chansel=ft_channelselection(cell({'*Rolandic*_L_*'  '*Temporal*_Sup*_L_*'  '*Postcentral*L*' '*Heschl*L*'}), label);

chansel=ft_channelselection(cell({ '*Temporal*'   '*Heschl*'}), label);

%%
%BRAINNETOEM ATLAS LABEL
% ROI posterior right
chansel=ft_channelselection(cell({'STG, Left Superior Temporal Gyrus A41/42*' 'PoG, Left Postcentral Gyrus A1/2/3tonIa, area 1/2/3 (tongue and larynx region)*' 'MTG, Left Middle Temporal Gyrus aSTS, anterior superior temporal sulcus*' 'STG, Left Superior Temporal Gyrus TE1.0 and TE1.2*' 'STG, Left Superior Temporal Gyrus A22c, caudal area 22*' 'STG, Left Superior Temporal Gyrus A41/42, area 41/42' }), label);

% ROI posterior middle
chansel=ft_channelselection(cell({'STG, Left Superior Temporal Gyrus A41/42*' 'PoG, Left Postcentral Gyrus A1/2/3tonIa, area 1/2/3 (tongue and larynx region)*' 'MTG, Left Middle Temporal Gyrus aSTS, anterior superior temporal sulcus*' 'STG, Left Superior Temporal Gyrus TE1.0 and TE1.2*' 'STG, Left Superior Temporal Gyrus A22c, caudal area 22*' 'STG, Left Superior Temporal Gyrus A41/42, area 41/42' }), label);


% ROI posterior middle
chansel=ft_channelselection(cell({'MTG, Left Middle Temporal Gyrus A37dl*' 'ITG, Left Inferior Temporal Gyrus A37vl*' 'IPL, Left Inferior Parietal Lobule A39rv*'}), label);

chansel=ft_channelselection(cell({'MTG, Right Middle Temporal Gyrus A37dl*' 'ITG, Right Inferior Temporal Gyrus A37vl*' 'IPL, Right Inferior Parietal Lobule A39rv*'}), label);

chansel=ft_channelselection(cell({'IFG, Right Inferior Frontal Gyrus A45r*' 'MFG, Right Middle Frontal Gyrus A9/46v*' 'IFG, Right Inferior Frontal Gyrus IFS*'}), label);

%%
chansel=ft_channelselection(cell({ 'Angular_L_*'}), label); % 'Parietal_Inf_L_*', 

%[C,ia]=setdiff(label, chansel);
[C,ia]=intersect(label, chansel);

[C2,ia2]=intersect(test2, ia);

%idx_inside2all = find(template_grid.inside);%important to apply find the indeces of all fields not only inside...
%ia_all = idx_inside2all(ia);


 voxels_label=label(C2);
  voxels_indl=ia2;
%%
newgrid=template_grid;
newgrid.inside(template_grid.inside==1) = 0;
newgrid.inside(ia_all) = 1; 
 
%% ------------------------extract individual voxel
for i=1:7
size(find(stat.posclusterslabelmat(:,i) ==1))
% try to separate very big cluster
end
%%
% stat.clus = (stat.posclusterslabelmat(:,:) == 1)
stat.clus = stat.mask == 1;
stat.clus2=(sum(stat.clus, 2)>=1);
voxels = stat.label(stat.clus2);

%%
voxels = stat.label(stat.mask);
%%
nsubj =  length(chan7);

for i = 1:nsubj
  
    cfg=[];
    cfg.frequency = [2 7];
     cfg.avgoverfreq='yes';
    cfg.channel= voxels %voxels_indl; %ia;%voxels;% 892 max stat value for visual, 519 max value in cluster; 903 for auditory (wichtigc leerzeichen wenn nur dreistellige zahl)
    cfg.avgoverchan='yes';
  
    
    as_novoc{i}=ft_selectdata(cfg, novoc{i});
    as_novoc{i}=as_novoc{i}.powspctrm;
    as_chan7{i}=ft_selectdata(cfg, chan7{i});
    as_chan7{i}=as_chan7{i}.powspctrm;
    as_chan5{i}=ft_selectdata(cfg, chan5{i});
    as_chan5{i}=as_chan5{i}.powspctrm;     
    
    as_chan3{i}=ft_selectdata(cfg, chan3{i});
    as_chan3{i}=as_chan3{i}.powspctrm;
    as_chan2{i}=ft_selectdata(cfg, chan2{i});
    as_chan2{i}=as_chan2{i}.powspctrm;
    as_chan1{i}=ft_selectdata(cfg, chan1{i});
    as_chan1{i}=as_chan1{i}.powspctrm;  
  
end

%%
v_novoc=cell2mat(as_novoc);
v_chan7=cell2mat(as_chan7);
v_chan5=cell2mat(as_chan5);
v_chan3=cell2mat(as_chan3);
v_chan2=cell2mat(as_chan2);
v_chan1=cell2mat(as_chan1);

figure;
boxplot([v_novoc', v_chan7', v_chan5', v_chan3', v_chan2', v_chan1'],'Labels',{'no','7-chan','5-chan','3-chan','2-chan','1-chan'})
hold on
plot([1,2,3,4,5,6], [mean(v_novoc),mean(v_chan7),mean(v_chan5),mean(v_chan3),mean(v_chan2),mean(v_chan1)], 'dg', 'MarkerSize', 6, 'LineWidth', 3);
% hold off

%%
as_novoc=cell2mat(as_novoc');
as_chan7=cell2mat(as_chan7');
as_chan5=cell2mat(as_chan5');
as_chan3=cell2mat(as_chan3');
as_chan2=cell2mat(as_chan2');
as_chan1=cell2mat(as_chan1');

%%
save('coh_1_15_Hz_fooof_yc_nohp.mat', 'as_novoc', 'as_chan7', 'as_chan5', 'as_chan3', 'as_chan2', 'as_chan1')
%%
for subj = 1:length(coh_all_nonoise)

cfg=[];
cfg.channel = find(stat.posclusterslabelmat == 1);
coh_cut_nn{subj} = ft_selectdata(cfg, coh_all_nonoise{subj});
coh_cut_ln{subj} = ft_selectdata(cfg, coh_all_lownoise{subj});
coh_cut_hn{subj} = ft_selectdata(cfg, coh_all_highnoise{subj});

end
%%

%% save 
all_v = [v_novoc; v_chan7; v_chan5; v_chan3; v_chan2; v_chan1];
all_vox = all_v';

%%
m_as_novoc=mean(as_novoc);
m_as_chan7=mean(as_chan7);
m_as_chan5=mean(as_chan5);
m_as_chan3=mean(as_chan3);
m_as_chan2=mean(as_chan2);
m_as_chan1=mean(as_chan1);

%%

s_as_novoc=std(as_novoc)/sqrt(length(as_novoc));
s_as_chan7=std(as_chan7)/sqrt(length(as_chan7));
s_as_chan5=std(as_chan5)/sqrt(length(as_chan5));
s_as_chan3=std(as_chan3)/sqrt(length(as_chan3));
s_as_chan2=std(as_chan2)/sqrt(length(as_chan2));
s_as_chan1=std(as_chan1)/sqrt(length(as_chan1));


%%

  binno_high=as_chan1;
  bin7_high=as_chan7;
  bin5_high=as_chan5;
  bin3_high=as_chan3;
  bin2_high=as_chan2;
  bin1_high=as_chan1;

  


%%
data_all1=(binno_high + bin7_high + bin5_high + bin3_high + bin2_high + bin1_high)./6


%% for polyfit
y_all1=[binno_high, bin7_high,  bin5_high,  bin3_high,  bin2_high,  bin1_high]

x_all1=[ones(length(binno_high),1)*1; ones(length(bin7_high),1)*2;  ones(length(bin5_high),1)*3;  ones(length(bin3_high),1)*4;  ones(length(bin2_high),1)*5;  ones(length(bin1_high),1)*6]
x_all1=x_all1

[t r]=fit(x_all1, y_all1', 'poly1')
%%
no_centered=binno_high - data_all1;
no7_centered=bin7_high - data_all1;
no5_centered=bin5_high - data_all1;
no3_centered=bin3_high - data_all1;
no2_centered=bin2_high - data_all1;
no1_centered=bin1_high - data_all1;

%%
  s_bin1_no=std(no1_centered)/sqrt(length(bin1_high));
  s_bin2_no=std(no2_centered)/sqrt(length(bin2_high));
  s_bin3_no=std(no3_centered)/sqrt(length(bin3_high));
  s_bin5_no=std(no5_centered)/sqrt(length(bin5_high));
  s_bin7_no=std(no7_centered)/sqrt(length(bin7_high));
  s_binno_no=std(no_centered)/sqrt(length(binno_high));
%%  
s_M_bin1_high=s_bin1_no*sqrt(6/5);
s_M_bin2_high=s_bin2_no*sqrt(6/5);
s_M_bin3_high=s_bin3_no*sqrt(6/5);
s_M_bin5_high=s_bin5_no*sqrt(6/5);
s_M_bin7_high=s_bin7_no*sqrt(6/5);
s_M_binno_high=s_binno_no*sqrt(6/5);




%%

mean_as=[m_as_novoc, m_as_chan7, m_as_chan5, m_as_chan3, m_as_chan2, m_as_chan1];

std_err_as=[s_M_binno_high, s_M_bin7_high, s_M_bin5_high, s_M_bin3_high, s_M_bin2_high, s_M_bin1_high];


%%

mean_as=[m_as_novoc, m_as_chan7, m_as_chan5, m_as_chan3, m_as_chan2, m_as_chan1];

std_err_as=[s_as_novoc, s_as_chan7, s_as_chan5, s_as_chan3, s_as_chan2, s_as_chan1];

%%
addpath('/mnt/obob/staff/ahauswald/useful_functions/')
figure1 = figure('Color',[1 1 1]);

% Create axes
axes1 = axes('Parent',figure1);

 barwitherr(std_err_as, mean_as)
 %%
 hold
 scatter((ones(length(as_novoc), 1))*1, as_novoc)
 scatter((ones(length(as_chan7), 1))*2, as_chan7)
 scatter((ones(length(as_chan5), 1))*3, as_chan5)
 scatter((ones(length(as_chan3), 1))*4, as_chan3)
 scatter((ones(length(as_chan2), 1))*5, as_chan2)
 scatter((ones(length(as_chan1), 1))*6, as_chan1)

 
 %title('Left Temporal 1-4 Hz');

% Set the remaining axes properties
set(axes1,'FontSize',16,'XTick',[1 2 3 4 5 6],'XTickLabel',...
  {'original','7-chan','5-chan','3-chan','2-chan', '1-chan'});
%%
% figure1 = figure('Color',[1 1 1]);
% axes1 = axes('Parent',figure1);
% hold all

for i=1:24
plot([1:1:6], [as_novoc(i), as_chan7(i), as_chan5(i), as_chan3(i), as_chan2(i), as_chan1(i)])
%pause
end

%%
plot([1:1:6], [mean(as_novoc), mean(as_chan7), mean(as_chan5), mean(as_chan3), mean(as_chan2), mean(as_chan1)])

%%
addpath('/mnt/obob/staff/ahauswald/useful_functions/')
%%
figure1 = figure('Color',[1 1 1]);
axes1 = axes('Parent',figure1);
hold all


for i=1:24

plot(GA_as_no.freq, nanmean(as_freq_highnoise{i}.powspctrm));
%xlim([0 20])
%plot(GA_as_low.freq, log10(nanmean(as_freq_lownoise{i}.powspctrm),'r'))
%plot(GA_as_high.freq, log10(nanmean(as_freq_nonoise{i}.powspctrm),'g'))
%box off
pause

end
%% get data for fooof
for subj = 1:length(novoc)

cfg=[];
cfg.channel = voxels;
coh_novoc{subj} = ft_selectdata(cfg, novoc{subj});
coh_chan7{subj} = ft_selectdata(cfg, chan7{subj});
coh_chan5{subj} = ft_selectdata(cfg, chan5{subj});
coh_chan3{subj} = ft_selectdata(cfg, chan3{subj});
coh_chan2{subj} = ft_selectdata(cfg, chan2{subj});
coh_chan1{subj} = ft_selectdata(cfg, chan1{subj});
end