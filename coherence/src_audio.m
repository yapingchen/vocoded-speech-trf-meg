function [coh_novoc, coh_chan7, coh_chan5, coh_chan3, coh_chan2, coh_chan1]=src_audio(cfg, data_ep)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%option to use ica
% output is source coherence

 lf=cfg.lf;
 grad=cfg.grad;
%%
if isfield(cfg, 'ica')
  comp=cfg.ica;
 
  n_comp=cfg.rej_comp;
end
%%
novoc = length(find(data_ep.trialinfo>=51 & data_ep.trialinfo<=74));
chan7 = length(find(data_ep.trialinfo>=81 & data_ep.trialinfo<=104));
chan5 = length(find(data_ep.trialinfo>=111 & data_ep.trialinfo<=134));
chan3 = length(find(data_ep.trialinfo>=141 & data_ep.trialinfo<=164));
chan2 = length(find(data_ep.trialinfo>=171 & data_ep.trialinfo<=194));
chan1 = length(find(data_ep.trialinfo>=201 & data_ep.trialinfo<=224));

nTrials = min([novoc,chan7,chan5, chan3, chan2, chan1]);

Trnovoc = [find(data_ep.trialinfo>=51 & data_ep.trialinfo<=74)];
Trnovoc =(sort(Trnovoc(randperm(length(Trnovoc),nTrials))));

Trchan7 = [find(data_ep.trialinfo>=81 & data_ep.trialinfo<=104)];
Trchan7 =(sort(Trchan7(randperm(length(Trchan7),nTrials))));

Trchan5 = [find(data_ep.trialinfo>=111 & data_ep.trialinfo<=134)];
Trchan5 =(sort(Trchan5(randperm(length(Trchan5),nTrials))));

Tr3chan = [find(data_ep.trialinfo>=141 & data_ep.trialinfo<=164)];
Tr3chan =(sort(Tr3chan(randperm(length(Tr3chan),nTrials))));

Trchan2 = [find(data_ep.trialinfo>=171 & data_ep.trialinfo<=194)];
Trchan2 =(sort(Trchan2(randperm(length(Trchan2),nTrials))));

Trchan1 = [find(data_ep.trialinfo>=201 & data_ep.trialinfo<=224)];
Trchan1 =(sort(Trchan1(randperm(length(Trchan1),nTrials))));

%% ICA correction of MEG channels (separate from env channel)

cfg=[];
cfg.channel={'speech_envelope'};
envelope=ft_selectdata(cfg, data_ep);

cfg.channel={'MEG'};
meg=ft_selectdata(cfg, data_ep);

grad=ft_convert_units(grad, 'm');
meg.grad=grad;
%%
if exist('comp')
cfg=[];
cfg.unmixing=comp.unmixing;
cfg.topolabel=comp.topolabel;
meg_comp=ft_componentanalysis(cfg,meg);

cfg=[];
cfg.component=n_comp;
data=ft_rejectcomponent(cfg, meg_comp);

meg=data;
end
clear data_ep

%% beam single trials
cfg=[];
cfg.covariance='yes';
data_tl_cov=ft_timelockanalysis(cfg, meg);

cfg = [];
cfg.grid = lf;
cfg.regfac = '10%';
filt = obob_svs_compute_spat_filters(cfg, data_tl_cov);

cfg = [];
cfg.spatial_filter = filt;
datasource = obob_svs_beamtrials_lcmv(cfg, meg);


%%
if isfield(datasource, 'cfg')
  datasource=rmfield(datasource, 'cfg');
end
%% append the envelope to the SRC time-series and select the 2 conditions
cfg=[];
cfg.appenddim='chan';
all_data = ft_appenddata(cfg, datasource, envelope);%irgendwas passiert hier...
all_data.trialinfo = datasource.trialinfo;

%% for the rest, keep the trials and compute coh
i=1458; % 1457+1 do only envelope
  cfg            = [];
  cfg.method     = 'mtmfft';
  cfg.foi    = [2:0.25:20];
  cfg.keeptrials = 'yes'; %needed for coherence, but blows up the data size!
  cfg.tapsmofrq  = 4;
  cfg.output     = 'fourier';
  
  cfg.trials = [Trnovoc] ;
  freq_novoc.(all_data.label{i})            = ft_freqanalysis(cfg, all_data);
  cfg.trials = [Trchan7] ;
  freq_chan7.(all_data.label{i})              = ft_freqanalysis(cfg, all_data);
  cfg.trials = [Trchan5];
  freq_chan5.(all_data.label{i})              = ft_freqanalysis(cfg, all_data);  
  cfg.trials = [Tr3chan] ;
  freq_3chan.(all_data.label{i})            = ft_freqanalysis(cfg, all_data);
  cfg.trials = [Trchan2] ;
  freq_chan2.(all_data.label{i})              = ft_freqanalysis(cfg, all_data);
  cfg.trials = [Trchan1];
  freq_chan1.(all_data.label{i})              = ft_freqanalysis(cfg, all_data);

  
  cfg            = [];
  cfg.method     = 'coh';
  cfg.channelcmb = {'all' all_data.label{i}};
  
  coh_novoc.(all_data.label{i})                = ft_connectivityanalysis(cfg, freq_novoc.(all_data.label{i}));
  coh_novoc.(all_data.label{i})  =rmfield(coh_novoc.(all_data.label{i}) , 'cfg');
  coh_chan7.(all_data.label{i})               = ft_connectivityanalysis(cfg, freq_chan7.(all_data.label{i}));
  coh_chan7.(all_data.label{i})  =rmfield(coh_chan7.(all_data.label{i}) , 'cfg');
  coh_chan5.(all_data.label{i})                = ft_connectivityanalysis(cfg, freq_chan5.(all_data.label{i}));
  coh_chan5.(all_data.label{i})  =rmfield(coh_chan5.(all_data.label{i}) , 'cfg'); 

  coh_chan3.(all_data.label{i})                = ft_connectivityanalysis(cfg, freq_3chan.(all_data.label{i}));
  coh_chan3.(all_data.label{i})  =rmfield(coh_chan3.(all_data.label{i}) , 'cfg');
  coh_chan2.(all_data.label{i})               = ft_connectivityanalysis(cfg, freq_chan2.(all_data.label{i}));
  coh_chan2.(all_data.label{i})  =rmfield(coh_chan2.(all_data.label{i}) , 'cfg');
  coh_chan1.(all_data.label{i})                = ft_connectivityanalysis(cfg, freq_chan1.(all_data.label{i}));
  coh_chan1.(all_data.label{i})  =rmfield(coh_chan1.(all_data.label{i}) , 'cfg');  

  clear freq*

%   cfg            = [];
%   cfg.method     = 'mtmfft';
%   cfg.foi    = [0:0.125:40];
%   cfg.keeptrials = 'no'; %needed for coherence, but blows up the data size!
%   cfg.tapsmofrq  = 4;
%   cfg.output     = 'pow';
%   
%   cfg.trials = [Trnovoc] ;
%   freq_novoc.(all_data.label{i})            = ft_freqanalysis(cfg, all_data);
%   cfg.trials = [Trchan7] ;
%   freq_chan7.(all_data.label{i})              = ft_freqanalysis(cfg, all_data);
%   cfg.trials = [Trchan5];
%   freq_chan5.(all_data.label{i})              = ft_freqanalysis(cfg, all_data);  
%   cfg.trials = [Tr3chan] ;
%   freq_3chan.(all_data.label{i})            = ft_freqanalysis(cfg, all_data);
%   cfg.trials = [Trchan2] ;
%   freq_chan2.(all_data.label{i})              = ft_freqanalysis(cfg, all_data);
%   cfg.trials = [Trchan1];
%   freq_chan1.(all_data.label{i})              = ft_freqanalysis(cfg, all_data);

end

