load('gmf.mat')
figure;
xlim_1 = -80; xlim_2 = 420;
colorcode = cell(1,6); 
colorcode{1} = '#440154FF'; colorcode{2} = '#414487FF'; colorcode{3} = '#2A788EFF';
colorcode{4} = '#22A884FF'; colorcode{5} = '#7AD151FF'; colorcode{6} = '#FDE725FF';
for cond_ind = [1:6]
  plot(gmf{1}.time, gmf{cond_ind}.avg, 'LineWidth',6, 'color', sscanf(colorcode{cond_ind}(2:end),'%2x%2x%2x', [1 3])/255);
  xlim([xlim_1,xlim_2]);
  hold on
end

title('Temporal Response Function');
legend('original','7-chan','5-chan','3-chan','2-chan','1-chan')
xlabel('Time (ms)')
ylabel('mean global field power')
box('off')
a = get(gca,'XTickLabel'); 

