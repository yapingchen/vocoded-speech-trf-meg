load('./data/trf_sensor_data.mat')

%% prepare neighbour for statistics
cfg_neighb        = [];
cfg_neighb.method = 'template'; 
cfg_neighb.template    = 'neuromag306cmb_neighb.mat';  
neighbour         = ft_prepare_neighbours(cfg_neighb);

%% run statistics
nsubj = length(trf_no_cmb);

cfg = [];
cfg.parameter   = 'avg';
cfg.statistic = 'depsamplesFunivariate';
cfg.method = 'montecarlo';
cfg.correctm = 'cluster';
cfg.latency = [50 110]; % [50 110; 175 230; 315 380] 
cfg.avgovertime = 'yes';
cfg.numrandomization = 1000;
cfg.clusteralpha     = 0.05; 
cfg.neighbours = neighbour;
cfg.minnbchan = 3;
cfg.design = [ones(1,nsubj) ones(1,nsubj)*2 ones(1,nsubj)*3 ones(1,nsubj)*4 ones(1,nsubj)*5 ones(1,nsubj)*6; 1:nsubj 1:nsubj 1:nsubj 1:nsubj 1:nsubj 1:nsubj];
cfg.ivar = 1;
cfg.uvar = 2;
cfg.tail = 1;
cfg.correcttail= 'prob'; 
cfg.alpha = 0.05;

stat = ft_timelockstatistics(cfg, trf_no_cmb{:}, trf_7ch_cmb{:},  trf_5ch_cmb{:}, trf_3ch_cmb{:}, trf_2ch_cmb{:}, trf_1ch_cmb{:}); 

%% topoplot
cfg = [];
cfg.highlightsymbolseries = ['*','*','.','.','.'];
cfg.highlightsizeseries = [7 7 6 6 6];
cfg.layout = 'neuromag306cmb_helmet.mat'; 
cfg.contournum = 0;
cfg.markersymbol = '.';
cfg.alpha = 0.05;
cfg.parameter='stat';
cfg.subplotsize = [1 1];
ft_clusterplot(cfg, stat);
colorbar