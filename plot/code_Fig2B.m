%% need fieldtrip functions
ft_defaults;

%% need some function and files for plot
addpath('plot_brain_surface')

%% load an mri
load('standard_mri.mat'); % from fieldtrip: 'template/headmodel/standard_mri.mat'
mri = ft_convert_units(mri,'m');

%% load statistics results
load('./data/TRF50_fstat.mat') % TRF200_fstat, TRF350_fstat for the other TRF results

%% Plot the statistics effect
cfg = [];
cfg.funparameter = 'stat';
cfg.maskparameter = 'mask';
cfg.method = 'surface';
cfg.projmethod = 'nearest'; % needed for method = 'surface'; 'project', 'sphere_avg', 'sphere_weighteddistance'
cfg.colorbar = 'no';
cfg.camlight = 'no';

% left hem
cfg.surffile = 'surf_caretleft.mat';
ft_sourceplot(cfg, src_fstat);
plot_caret_style('TRF_left', 400, [], 'left');

% right hem
cfg.surffile = 'surf_caretright.mat';
ft_sourceplot(cfg, src_fstat);
plot_caret_style('TRF_right', 400, [], 'right');

% right inside
inverse_r = [-90, 0];
inverse_l = [90, 0];

cfg.surffile = 'surf_caretright.mat';
ft_sourceplot(cfg, src_fstat);
plot_caret_style('TRF_in_right', 400, [], inverse_r);

cfg.surffile = 'surf_caretleft.mat';
ft_sourceplot(cfg, src_fstat);
plot_caret_style('TRF_in_left', 400, [], inverse_l);


