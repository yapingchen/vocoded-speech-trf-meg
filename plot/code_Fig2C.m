load('trf_sensor_data.mat')

%% prepare neighbour for statistics
% load('grad.mat')
% data_dummy{1}.grad = grad;
cfg_neighb        = [];
cfg_neighb.method = 'template'; 
cfg_neighb.template    = 'neuromag306cmb_neighb.mat';  
neighbour         = ft_prepare_neighbours(cfg_neighb);
%% run statistics
nsubj = length(trf_no_cmb);

cfg = [];
cfg.parameter   = 'avg';
cfg.statistic = 'depsamplesFunivariate';
cfg.method = 'montecarlo';
cfg.correctm = 'cluster';
cfg.latency = [50 110]; % [50 110; 175 230; 315 380] 
cfg.avgovertime = 'yes';
cfg.numrandomization = 1000;
cfg.clusteralpha     = 0.05; 
cfg.neighbours = neighbour;
cfg.minnbchan = 3;
cfg.design = [ones(1,nsubj) ones(1,nsubj)*2 ones(1,nsubj)*3 ones(1,nsubj)*4 ones(1,nsubj)*5 ones(1,nsubj)*6; 1:nsubj 1:nsubj 1:nsubj 1:nsubj 1:nsubj 1:nsubj];
cfg.ivar = 1;
cfg.uvar = 2;
cfg.tail = 1;
cfg.correcttail= 'prob'; 
cfg.alpha = 0.05;

stat = ft_timelockstatistics(cfg, trf_no_cmb{:}, trf_7ch_cmb{:},  trf_5ch_cmb{:}, trf_3ch_cmb{:}, trf_2ch_cmb{:}, trf_1ch_cmb{:}); 

[rows,cols,vals] = find(stat.prob < 0.05);
stat.time(unique(cols))


%% define channels to extract
stat.clus = stat.mask == 1;
stat.vox=(sum(stat.clus, 2)>=1);
channels = stat.label(stat.vox);

%% extract TRF values from clusters
nsubj =  length(trf_no_cmb);

for i = 1:nsubj
  
    cfg=[];
    cfg.latency = [50 110]; % [50 110; 175 230; 315 380] 
    cfg.avgovertime='yes';
    
    cfg.channel=channels;%voxels;% 892 max stat value for visual, 519 max value in cluster; 903 for auditory (wichtigc leerzeichen wenn nur dreistellige zahl)
    cfg.avgoverchan='yes';
  
    as_novoc{i}=ft_selectdata(cfg, trf_no_cmb{i});
    as_novoc{i}=as_novoc{i}.avg;
    as_chan7{i}=ft_selectdata(cfg, trf_7ch_cmb{i});
    as_chan7{i}=as_chan7{i}.avg;
    as_chan5{i}=ft_selectdata(cfg, trf_5ch_cmb{i});
    as_chan5{i}=as_chan5{i}.avg;     
    
    as_chan3{i}=ft_selectdata(cfg, trf_3ch_cmb{i});
    as_chan3{i}=as_chan3{i}.avg;
    as_chan2{i}=ft_selectdata(cfg, trf_2ch_cmb{i});
    as_chan2{i}=as_chan2{i}.avg;
    as_chan1{i}=ft_selectdata(cfg, trf_1ch_cmb{i});
    as_chan1{i}=as_chan1{i}.avg;  
  
end
%%
v_novoc=cell2mat(as_novoc);
v_chan7=cell2mat(as_chan7);
v_chan5=cell2mat(as_chan5);
v_chan3=cell2mat(as_chan3);
v_chan2=cell2mat(as_chan2);
v_chan1=cell2mat(as_chan1);

%% plot

figure;
boxplot([v_novoc', v_chan7', v_chan5', v_chan3', v_chan2', v_chan1'],'Labels',{'no','7-chan','5-chan','3-chan','2-chan','1-chan'})

hold on
plot([1,2,3,4,5,6], [mean(v_novoc),mean(v_chan7),mean(v_chan5),mean(v_chan3),mean(v_chan2),mean(v_chan1)], 'dg', 'MarkerSize', 6, 'LineWidth', 3);