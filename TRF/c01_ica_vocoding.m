function c01_ica_vocoding(subject)
%% DO ICA

%%  init obob_ownft...
addpath('/mnt/obob/obob_ownft');
cfg = [];
cfg.package.hnc_condor = true;
obob_init_ft(cfg);
outdir = '/mnt/obob/staff/ychen/vocoding_5levels/data/ica_8Hz/';
%%
data_ep=functions.preproc_ICA_lag16(subject);

cfg=[];
cfg.channel={'MEG'};
meg=ft_selectdata(cfg, data_ep);

clear data_ep
%% decompose the data into independent components:
cfg=[];
cfg.method = 'runica';
cfg.runica.pca = 50;
comp = ft_componentanalysis(cfg, meg);

%% Save your ica_components
out_fname = fullfile(outdir, sprintf('ica_%s.mat', subject));
save(out_fname,'comp'); % save 'ica_components'


end