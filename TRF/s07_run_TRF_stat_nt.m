%% Do forward model (TRF) stat
clear all; clc; close all;

%% Init obob_ownft & add path to custom functions
addpath('/mnt/obob/obob_ownft');
cfg = [];
obob_init_ft(cfg);
cd '/mnt/obob/staff/ychen/vocoding_5levels/analysis/'
addpath('/mnt/obob/staff/ychen/vocoding_5levels/analysis/')

%% load subj_list
% get raw_data paths & filenames
study_acronym = 'yc_audiobook_vocoding';
all_subjects = functions.subjects_list(study_acronym);

%% load data
fm_folder = '/mnt/obob/staff/ychen/vocoding_5levels/data/trf_2s_cond_env_8Hz_clean_s1000_nt/';

% load
for i = 1:numel(all_subjects)
  data_tmp{i} = load(fullfile(fm_folder, sprintf('%s_trf.mat', all_subjects{i})));
end

%% extract data
for i = 1:numel(all_subjects)
  data_dummy{i} = data_tmp{i}.data_erf;
  fem_no{i} = data_tmp{i}.fem_no;
  fem_7ch{i} = data_tmp{i}.fem_7ch;
  fem_5ch{i} = data_tmp{i}.fem_5ch;
  fem_3ch{i} = data_tmp{i}.fem_3ch;
  fem_2ch{i} = data_tmp{i}.fem_2ch;
  fem_1ch{i} = data_tmp{i}.fem_1ch;
end

%%% fill in weights and time
trf_no = data_dummy;trf_7ch = data_dummy;trf_5ch = data_dummy;
trf_3ch = data_dummy;trf_2ch = data_dummy;trf_1ch = data_dummy;

load grad
for i = 1:numel(all_subjects)
  trf_no_avg{i} = transpose(squeeze(fem_no{i}.w));
  trf_no{i}.avg = trf_no_avg{i};
  trf_no{i}.time = fem_no{i}.t;
  trf_no{i}.grad = grad;
  trf_no{i} = rmfield(trf_no{i}, 'var');
  trf_no{i} = rmfield(trf_no{i}, 'dof');
  cfg = [];
  cfg.baseline     = [-40 0];
  trf_no_bc{i} = ft_timelockbaseline(cfg, trf_no{i});
  
  trf_7ch_avg{i} = transpose(squeeze(fem_7ch{i}.w));
  trf_7ch{i}.avg = trf_7ch_avg{i};
  trf_7ch{i}.time = fem_7ch{i}.t;
  trf_7ch{i}.grad = grad;
  trf_7ch{i} = rmfield(trf_7ch{i}, 'var');
  trf_7ch{i} = rmfield(trf_7ch{i}, 'dof');
  trf_7ch_bc{i} = ft_timelockbaseline(cfg, trf_7ch{i});
  
  trf_5ch_avg{i} = transpose(squeeze(fem_5ch{i}.w));
  trf_5ch{i}.avg = trf_5ch_avg{i};
  trf_5ch{i}.time = fem_5ch{i}.t;
  trf_5ch{i}.grad = grad;
  trf_5ch{i} = rmfield(trf_5ch{i}, 'var');
  trf_5ch{i} = rmfield(trf_5ch{i}, 'dof');
  trf_5ch_bc{i} = ft_timelockbaseline(cfg, trf_5ch{i});
  
  trf_3ch_avg{i} = transpose(squeeze(fem_3ch{i}.w));
  trf_3ch{i}.avg = trf_3ch_avg{i};
  trf_3ch{i}.time = fem_3ch{i}.t;
  trf_3ch{i}.grad = grad;
  trf_3ch{i} = rmfield(trf_3ch{i}, 'var');
  trf_3ch{i} = rmfield(trf_3ch{i}, 'dof');
  trf_3ch_bc{i} = ft_timelockbaseline(cfg, trf_3ch{i});
  
  trf_2ch_avg{i} = transpose(squeeze(fem_2ch{i}.w));
  trf_2ch{i}.avg = trf_2ch_avg{i};
  trf_2ch{i}.time = fem_2ch{i}.t;
  trf_2ch{i}.grad = grad;
  trf_2ch{i} = rmfield(trf_2ch{i}, 'var');
  trf_2ch{i} = rmfield(trf_2ch{i}, 'dof');
  trf_2ch_bc{i} = ft_timelockbaseline(cfg, trf_2ch{i});
  
  trf_1ch_avg{i} = transpose(squeeze(fem_1ch{i}.w));
  trf_1ch{i}.avg = trf_1ch_avg{i};
  trf_1ch{i}.time = fem_1ch{i}.t;
  trf_1ch{i}.grad = grad;
  trf_1ch{i} = rmfield(trf_1ch{i}, 'var');
  trf_1ch{i} = rmfield(trf_1ch{i}, 'dof');
  trf_1ch_bc{i} = ft_timelockbaseline(cfg, trf_1ch{i});
end

%%% ft_combineplanar
for n = 1:length(trf_no)
cfg = [];
cfg.method = 'sum';
trf_no_cmb{n} = ft_combineplanar(cfg, trf_no_bc{n});
trf_7ch_cmb{n} = ft_combineplanar(cfg, trf_7ch_bc{n});
trf_5ch_cmb{n} = ft_combineplanar(cfg, trf_5ch_bc{n});
trf_3ch_cmb{n} = ft_combineplanar(cfg, trf_3ch_bc{n});
trf_2ch_cmb{n} = ft_combineplanar(cfg, trf_2ch_bc{n});
trf_1ch_cmb{n} = ft_combineplanar(cfg, trf_1ch_bc{n});
end

%% create neighbor
load('grad.mat')
data_dummy{1}.grad = grad;
cfg_neighb        = [];
cfg_neighb.method = 'template'; % 'template, 'distance'
cfg_neighb.template    = 'neuromag306cmb_neighb.mat';  % neuromag306planar_neighb, neuromag306mag_neighb, neuromag306cmb_neighb
neighbour         = ft_prepare_neighbours(cfg_neighb);

%% ft_timelockstatistics
%%% load('trf_sensor_data.mat')

nsubj = length(trf_no_cmb);

cfg = [];
cfg.parameter   = 'avg';
cfg.statistic = 'depsamplesFunivariate';% 'depsamplesregrT' 
% cfg.statistic = 'ft_statfun_depsamplesT';
cfg.method = 'montecarlo';
cfg.correctm = 'cluster';
cfg.latency = [0 400]; 
cfg.avgovertime = 'no';
cfg.numrandomization = 10000;
cfg.clusteralpha     = 0.05; 
cfg.neighbours = neighbour;
cfg.minnbchan = 3;
cfg.design = [ones(1,nsubj) ones(1,nsubj)*2 ones(1,nsubj)*3 ones(1,nsubj)*4 ones(1,nsubj)*5 ones(1,nsubj)*6; 1:nsubj 1:nsubj 1:nsubj 1:nsubj 1:nsubj 1:nsubj];
cfg.ivar = 1;
cfg.uvar = 2;
cfg.tail = 1;
cfg.correcttail= 'prob'; 
cfg.alpha = 0.05;

stat = ft_timelockstatistics(cfg, trf_no_cmb{:}, trf_7ch_cmb{:},  trf_5ch_cmb{:}, trf_3ch_cmb{:}, trf_2ch_cmb{:}, trf_1ch_cmb{:}); 

[rows,cols,vals] = find(stat.prob < 0.05);
stat.time(unique(cols))


%% define channels to extract
stat.clus = stat.mask == 1;
stat.vox=(sum(stat.clus, 2)>=1);
channels = stat.label(stat.vox);

%% extract TRF values from clusters
nsubj =  length(trf_no_cmb);

for i = 1:nsubj
  
    cfg=[];
    cfg.latency = [50 110]; % [50 110; 175 230; 315 380] 
    cfg.avgovertime='yes';
    
    cfg.channel=channels;%voxels;% 892 max stat value for visual, 519 max value in cluster; 903 for auditory (wichtigc leerzeichen wenn nur dreistellige zahl)
    cfg.avgoverchan='yes';
  
    as_novoc{i}=ft_selectdata(cfg, trf_no_cmb{i});
    as_novoc{i}=as_novoc{i}.avg;
    as_chan7{i}=ft_selectdata(cfg, trf_7ch_cmb{i});
    as_chan7{i}=as_chan7{i}.avg;
    as_chan5{i}=ft_selectdata(cfg, trf_5ch_cmb{i});
    as_chan5{i}=as_chan5{i}.avg;     
    
    as_chan3{i}=ft_selectdata(cfg, trf_3ch_cmb{i});
    as_chan3{i}=as_chan3{i}.avg;
    as_chan2{i}=ft_selectdata(cfg, trf_2ch_cmb{i});
    as_chan2{i}=as_chan2{i}.avg;
    as_chan1{i}=ft_selectdata(cfg, trf_1ch_cmb{i});
    as_chan1{i}=as_chan1{i}.avg;  
  
end
%%
v_novoc=cell2mat(as_novoc);
v_chan7=cell2mat(as_chan7);
v_chan5=cell2mat(as_chan5);
v_chan3=cell2mat(as_chan3);
v_chan2=cell2mat(as_chan2);
v_chan1=cell2mat(as_chan1);

%% plot

figure;
boxplot([v_novoc', v_chan7', v_chan5', v_chan3', v_chan2', v_chan1'],'Labels',{'no','7-chan','5-chan','3-chan','2-chan','1-chan'})

hold on
plot([1,2,3,4,5,6], [mean(v_novoc),mean(v_chan7),mean(v_chan5),mean(v_chan3),mean(v_chan2),mean(v_chan1)], 'dg', 'MarkerSize', 6, 'LineWidth', 3);


